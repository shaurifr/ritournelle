[![Codacy Badge](https://api.codacy.com/project/badge/Grade/964dc051dc564acb96ffcbb97a5f9869)](https://www.codacy.com/gl/shaurifr/ritournelle?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=shaurifr/ritournelle&amp;utm_campaign=Badge_Grade)

L'application est live à l'adresse [https://www.ritournelle.eu](https://www.ritournelle.eu)

# Ritournelle

Pour tester cette application :

1. Configurer l'accès à la base de données dans le fichier `.env`
1. Créer la BDD : `php bin/console doctrine:database:create`
1. Créer le schéma : `php bin/console doctrine:migrations:migrate`
1. Charger les Fixtures : `php bin/console doctrine:fixtures:load`
1. Lancer le server de test : `symfony server:start` ou `php bin/console server:run` 
1. Se connecter en tant qu'admin avec les identifiants `carlos` / `carlos`
1. Se connecter en tant que user avec les identifiants `carlos2` / `carlos2`

## Création du projet

`composer create-project symfony/skeleton "ritournelle" "3.4.*"`

## Installation des composants

`composer require security-csrf`  
`composer require validator`  
`composer require form`  
`composer require doctrine`  
`composer require asset`  
`composer require annotations`  
`composer require twig`  
`composer require apache-pack` _si vous utilisez apache_  
`composer require --dev debug`  
`composer require --dev maker`  
`composer require --dev var-dumper`  
`composer require --dev doctrine/doctrine-fixtures-bundle`  
`composer require symfony/swiftmailer-bundle`  

## Utilisation de make

Génération d'entités :  
`php bin/console make:entity`

Génération de controller :  
`php bin/console make:controller`  
Cela générera aussi un template.  

Génération de form :  
`php bin/console make:form`

## BDD

La configuration de l'accès à la BDD se fait dans le fichier `.env`, voir `DATABASE_URL`.  

Création du schéma :  
`php bin/console doctrine:database:create`

Génération du fichier de migration :  
`php bin/console make:migration`

Execution du fichier de migration :  
`php bin/console doctrine:migrations:migrate`

Vérification que la BDD est à jour avec les entités :  
`php bin/console doctrine:schema:validate`

Load des Fixtures pour initialiser la BDD pour le développement :
`php bin/console doctrine:fixtures:load`

## i18n

Génération des fichiers de traductions au format xliff pour le français avec la commande :  
`php bin/console translation:update --dump-messages --force --output-format xlf fr`  

## Utilisation du serveur intégré

Le composant `server` permet d'avoir un server qui va servir les pages. Cela évite d'installer apache.  
`php bin/console server:start`  

    Sous windows, cette commande ne fonctionne pas. 
    Il faut utiliser à la place `php bin/console server:run`

   ⚠️ Cette commande disparait avec symfony 5.
