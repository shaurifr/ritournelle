<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEmailType;
use App\Form\UserProfileType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    const PLAY_NEXT = 'play';
    const CHANGE_NEXT = 'change';
    const CHANGE_WARN_NEXT = 'change_warn';
    const RANKING_NEXT = 'ranking';

    /**
     * @Route("/register", name="register")
     *
     * @return Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session
    ) {
        $user = new User();
        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            // Encode le password
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            // on connecte le nouvel utilisateur
            $token = new UsernamePasswordToken(
                $user,
                $password,
                'main',
                $user->getRoles()
            );
            $tokenStorage->setToken($token);
            $session->set('_security_main', serialize($token));

            return $this->redirectToRoute('ritournelle_enigma');
        }

        return $this->render(
            'user/register.html.twig',
            [
                'userForm' => $userForm->createView(),
            ]
        );
    }

    /**
     * @Route("/connect", name="connect")
     *
     * @return Response
     */
    public function connect(
        Request $request,
        AuthenticationUtils $authenticationUtils,
        UserManager $userManager,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        if ($this->getUser()) {
            // s'il est déjà connecté, on retourne vers la page pour jouer
            return $this->redirectToRoute('ritournelle_enigma');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $userEmailForm = $this->createForm(UserEmailType::class);
        $userEmailForm->handleRequest($request);
        if ($userEmailForm->isSubmitted()) {
            if ($userEmailForm->isValid()) {
                $email = $userEmailForm->getData()['email'];
                /** @var UserRepository $userRepository */
                $userRepository = $entityManager->getRepository(User::class);
                $user = $userRepository->findOneByEmail($email);
                if ($user) {
                    try {
                        // on envoie le mail demandant le reset du mot de passe
                        $userManager->sendChangePassword($user);
                        $this->addFlash('success', 'user.check.email.change.password');
                    } catch (\Exception $exception) {
                        $logger->error(
                            'The email for changing the password could not be sent',
                            [
                                'email' => $email,
                                'error' => $exception->getMessage(),
                                'trace' => $exception->getTraceAsString(),
                            ]
                        );
                    }
                } else {
                    $this->addFlash('warning', 'Cet email est inconnu.');
                }
            } else {
                $this->addFlash('danger', 'Veuillez vérifier vos données.');
            }
        }

        return $this->render(
            'user/connect.html.twig',
            [
                'last_username' => $lastUsername,
                'error' => $error,
                'showModal' => $userEmailForm->isSubmitted(),
                'userEmailForm' => $userEmailForm->createView(),
            ]
        );
    }

    /**
     * @Route("/users", name="user_list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function list(EntityManagerInterface $em)
    {
        $userRepo = $em->getRepository(User::class);
        $users = $userRepo->findAll();

        return $this->render('user/list.html.twig', compact('users'));
    }

    /**
     * @Route("/profile/{id}", name="user_profile")
     *
     * @return Response
     */
    public function profile(
        User $user,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session
    ) {
        if ($user != $this->getUser()) {
            throw $this->createAccessDeniedException('That\'s not your profile!');
        }
        $userProfileForm = $this->createForm(UserProfileType::class, $user);
        $userProfileForm->handleRequest($request);
        if ($userProfileForm->isSubmitted() && $userProfileForm->isValid()) {
            if ($user->hasPasswordChanged()) {
                // Encode le password
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);

                // on reconnecte l'utilisateur
                $token = new UsernamePasswordToken(
                    $user,
                    $password,
                    'main',
                    $user->getRoles()
                );
                $tokenStorage->setToken($token);
                $session->set('_security_main', serialize($token));
            }

            $em->persist($user);
            $em->flush();
        }

        return $this->render(
            'user/profile.html.twig',
            [
                'userProfileForm' => $userProfileForm->createView(),
            ]
        );
    }

    /**
     * @Route("/ranking_connect/{token}", name="ranking_connect")
     * @Route("/change_notification_ritournelle/{token}", name="change_notification")
     * @Route("/magik_connect/{token}", name="magik_link")
     * @Route("/password/{token}", name="change_password")
     */
    public function changePassword(
        $token,
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        LoggerInterface $logger
    ) {
        if (!$token) {
            return $this->redirectToRoute('ritournelle_enigma');
        }
        $decode = unserialize(base64_decode($token));
        if (!is_array($decode)) {
            return $this->redirectToRoute('ritournelle_enigma');
        }
        $user = $entityManager->getRepository(User::class)->find($decode['id']);
        if (!$user) {
            return $this->redirectToRoute('ritournelle_enigma');
        }
        $next = $decode['next'] ?? self::CHANGE_NEXT;
        if ($decode['token'] != $user->getToken()) {
            if (self::CHANGE_WARN_NEXT == $next || self::CHANGE_WARN_NEXT == $next) {
                return $this->redirectToRoute('user_profile', ['id' => $user->getId()]);
            }

            return $this->redirectToRoute('ritournelle_enigma');
        }

        // on logue le user et on le redirige vers sa page de profile
        $token = new UsernamePasswordToken(
            $user,
            $user->getPassword(),
            'main',
            $user->getRoles()
        );
        $tokenStorage->setToken($token);
        $session->set('_security_main', serialize($token));

        $user->setToken(null);
        $entityManager->flush();

        if (self::RANKING_NEXT == $next) {
            // on redirige vers la page pour voir le classement
            return $this->redirectToRoute('game_classement');
        }
        if (self::PLAY_NEXT == $next) {
            // on redirige vers la page pour jouer
            return $this->redirectToRoute('ritournelle_enigma');
        }
        if (self::CHANGE_WARN_NEXT == $next) {
            $this->addFlash('info', 'user.warn.change.invited');
        } else {
            $this->addFlash('info', 'user.password.change.invited');
        }
        // on redirige vers la page de profile

        return $this->redirectToRoute('user_profile', ['id' => $user->getId()]);
    }
}
