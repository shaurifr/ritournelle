<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\GameManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game", name="game_")
 */
class GameController extends AbstractController
{
    /**
     * @Route("/", name="classement")
     */
    public function classement(
        GameManager $gameManager
    ) {
        // On récupère le classement de la semaine en cours
        $firstCurrentWeek = $gameManager->getFirstCurrentWeek();
        $secondCurrentWeek = $gameManager->getSecondCurrentWeek();
        $thirdCurrentWeek = $gameManager->getThirdCurrentWeek();
        $othersCurrentWeek = $gameManager->getOthersCurrentWeek();

        // On récupère le classement du mois en cours
        $firstCurrentMonth = $gameManager->getFirstCurrentMonth();
        $secondCurrentMonth = $gameManager->getSecondCurrentMonth();
        $thirdCurrentMonth = $gameManager->getThirdCurrentMonth();
        $othersCurrentMonth = $gameManager->getOthersCurrentMonth();

        return $this->render(
            'game/classement.html.twig',
            compact(
                'firstCurrentWeek',
                'secondCurrentWeek',
                'thirdCurrentWeek',
                'othersCurrentWeek',
                'firstCurrentMonth',
                'secondCurrentMonth',
                'thirdCurrentMonth',
                'othersCurrentMonth'
            ));
    }

    /**
     * @Route("/notify/{id}", name="notify")
     */
    public function notify(
        User $user,
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        if ($user != $this->getUser()) {
            throw $this->createAccessDeniedException('That\'s not your game!');
        }

        $result = $user->getTodaysResult();

        $notify = ('true' == $request->get('notify'));

        $result->setCustomRitournelleNotify($notify);
        $entityManager->flush();

        return $this->json(['notification_activated' => $notify]);
    }
}
