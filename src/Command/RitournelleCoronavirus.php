<?php

namespace App\Command;

use App\Controller\UserController;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment as Twig_Environment;

class RitournelleCoronavirus extends Command
{
    protected static $defaultName = 'ritournelle:coronavirus';

    /** @var LoggerInterface */
    private $logger;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var \Swift_Mailer */
    private $mailer;
    /** @var Twig_Environment */
    private $twigEnvironment;
    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        Twig_Environment $twigEnvironment,
        TranslatorInterface $translator
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->twigEnvironment = $twigEnvironment;
        $this->translator = $translator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('tells every player to play during coronavirus')
            ->addArgument(
                'user',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'List of User to send the email'
            )//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $usersId = $input->getArgument('user');

//        $this->translator->setLocale("fr");
        // On récupère la liste des utilisateurs
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        if (!empty($usersId)) {
            $users = $userRepository->findById($usersId);
        } else {
            $users = $userRepository->findBy(['acceptedWarn' => true]);
        }
        foreach ($users as $user) {
            $html = $this->twigEnvironment->render(
                'emails/coronavirus.html.twig',
                [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'token' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::PLAY_NEXT])),
                ]
            );
            // On envoie un mail à chaque utilisateur
            $message = (new \Swift_Message($this->translator->trans('mail.coronavirus.subject', [], 'mail')))
                ->setFrom('contact@notifications.ritournelle.eu', 'Carlos')
                ->setReplyTo('carlos@shauri.fr')
                ->setBcc('carlos@shauri.fr')
                ->setTo($user->getEmail())
                ->setBody(
                    $html,
                    'text/html'
                )
                /* If you also want to include a plaintext version of the message */
                ->addPart(
                    Html2Text::convert($html),
                    'text/plain'
                );
            $this->mailer->send($message);
            $io->note('un email a été envoyé à '.$user->getName());
        }

        $io->success(count($users).' mails ont été envoyés.');

        return 0;
    }
}
