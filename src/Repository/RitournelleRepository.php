<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Ritournelle;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * @method Ritournelle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ritournelle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ritournelle[]    findAll()
 * @method Ritournelle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Ritournelle[]    findByUser(User $user)
 */
class RitournelleRepository extends EntityRepository
{
    /**
     * @return Ritournelle[] Returns an array of Ritournelle objects not played since $since
     *
     * @throws \Exception
     */
    public function findNotPlayedSince(string $since)
    {
        $q = $this->createQueryBuilder('r')
            ->select('IDENTITY(g.ritournelle)')
            ->innerJoin(Game::class, 'g', 'WITH', 'g.ritournelle = r.id')
            ->andWhere('g.playedAt > :since');
        $query = $this->createQueryBuilder('r2');
        $query
            ->where($query->expr()->notIn('r2.id', $q->getDQL()))
            ->andWhere('r2.enabled = true')
            ->andWhere('r2.user <> 1') // on enlève les ritournelles de l'admin
            ->setParameter('since', new \DateTime($since));

        $result = $query->getQuery()->getResult();

        return $result;
    }

    /**
     * @return Ritournelle[] Returns an array of Ritournelle objects not played since $since and played less than $times times
     *
     * @throws \Exception
     */
    public function findPlayedLessThanXTimesAndNotPlayedSince(int $times, string $since)
    {
        $q = $this->createQueryBuilder('r0')
            ->select('IDENTITY(g0.ritournelle)')
            ->innerJoin(Game::class, 'g0', 'WITH', 'g0.ritournelle = r0.id')
            ->andWhere('g0.playedAt > :since');
        $query = $this->createQueryBuilder('r');
        $query
            ->leftJoin(Game::class, 'g', 'WITH', 'g.ritournelle = r.id')
            ->where($query->expr()->notIn('r.id', $q->getDQL()))
            ->andWhere('r.enabled = true')
            ->groupBy('r')
            ->having('COUNT(r) <= :times')
            ->setParameter('times', $times)
            ->setParameter('since', new \DateTime($since));

        return $query->getQuery()->getResult();
    }
}
