<?php

namespace App\Repository;

use App\Entity\Game;
use Doctrine\ORM\EntityRepository;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends EntityRepository
{
    /**
     * @return Game|null
     *
     * @throws \Exception
     */
    public function getPreviousGame()
    {
        $query = $this->createQueryBuilder('g')
            ->andWhere('g.playedAt < :today')
            ->orderBy('g.playedAt', 'desc')
            ->setParameter('today', new \DateTime('today'));
        $results = $query->getQuery()->getResult();

        return array_shift($results);
    }
}
