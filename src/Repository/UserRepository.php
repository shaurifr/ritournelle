<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User|null findOneByEmail(string $email)
 * @method User[]    findAll()
 * @method User[]    findById(array $usersId)
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository implements UserLoaderInterface
{
    /**
     * @param string $usernameOrEmail
     *
     * @return mixed|\Symfony\Component\Security\Core\User\UserInterface|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($usernameOrEmail)
    {
        return $this->getEntityManager()->createQuery(
            'SELECT u
                FROM App\Entity\User u
                WHERE u.username = :query
                OR u.email = :query'
        )
            ->setParameter('query', $usernameOrEmail)
            ->getOneOrNullResult();
    }

    /**
     * @param string $time
     *
     * @return User[]|null
     *
     * @throws \Exception
     */
    public function findToBeNotified($time = 'today')
    {
        return $query = $this->createQueryBuilder('u')
            ->innerJoin('u.results', 'r')
            ->where('r.replyAt > :date')
            ->andWhere('r.customRitournelleNotify is not null')
            ->andWhere('r.customRitournelleNotify = true')
            ->setParameter(':date', new \DateTime($time))
            ->getQuery()
            ->getResult();
    }
}
