<?php

namespace App\Repository;

use App\Entity\JokerPlayed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JokerPlayed|null find($id, $lockMode = null, $lockVersion = null)
 * @method JokerPlayed|null findOneBy(array $criteria, array $orderBy = null)
 * @method JokerPlayed[]    findAll()
 * @method JokerPlayed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JokerPlayedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JokerPlayed::class);
    }

    // /**
    //  * @return JokerPlayed[] Returns an array of JokerPlayed objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JokerPlayed
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
