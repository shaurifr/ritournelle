<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107104103 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, ritournelle_id INT NOT NULL, played_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_232B318CD36A3CD6 (ritournelle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CD36A3CD6 FOREIGN KEY (ritournelle_id) REFERENCES ritournelle (id)');
        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC113D36A3CD6');
        $this->addSql('DROP INDEX IDX_136AC113D36A3CD6 ON result');
        $this->addSql('ALTER TABLE result CHANGE ritournelle_id game_id INT NOT NULL');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_136AC113E48FD905 ON result (game_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC113E48FD905');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP INDEX IDX_136AC113E48FD905 ON result');
        $this->addSql('ALTER TABLE result CHANGE game_id ritournelle_id INT NOT NULL');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113D36A3CD6 FOREIGN KEY (ritournelle_id) REFERENCES ritournelle (id)');
        $this->addSql('CREATE INDEX IDX_136AC113D36A3CD6 ON result (ritournelle_id)');
    }
}
