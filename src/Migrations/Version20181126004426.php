<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181126004426 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ritournelle ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ritournelle ADD CONSTRAINT FK_23A24BC9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_23A24BC9A76ED395 ON ritournelle (user_id)');
        $this->addSql('ALTER TABLE result CHANGE reply_at reply_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result CHANGE reply_at reply_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ritournelle DROP FOREIGN KEY FK_23A24BC9A76ED395');
        $this->addSql('DROP INDEX IDX_23A24BC9A76ED395 ON ritournelle');
        $this->addSql('ALTER TABLE ritournelle DROP user_id');
    }
}
