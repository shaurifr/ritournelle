<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("username", message="validators.same_username")
 * @UniqueEntity("email", message="validators.same_email")
 */
class User implements UserInterface
{
    const POINTS_BASE = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Length(max="250")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="25")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="30")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var Result[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Result", mappedBy="user", orphanRemoval=true)
     */
    private $results;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = ['ROLE_USER'];

    /**
     * Cette variable indique si on peut prévenie l'utilisateur chaque matin d'une nouvelle ritournelle.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $acceptedWarn = true;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $token;

    /**
     * @var Ritournelle[]
     * @ORM\OneToMany(targetEntity="App\Entity\Ritournelle", mappedBy="user", orphanRemoval=true)
     */
    private $ritournelles;

    /**
     * @var CustomRitournelle[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CustomRitournelle", mappedBy="user", orphanRemoval=true)
     */
    private $customRitournelles;

    /**
     * @var CustomResult[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CustomResult", mappedBy="user", orphanRemoval=true)
     */
    private $customResults;

    /**
     * @var bool
     */
    private $passwordChanged = false;

    /**
     * @var Response[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Response", mappedBy="user", orphanRemoval=true)
     */
    private $responses;

    /**
     * @var array
     *
     * Représente un tableau avec toutes les réponse organisé par id de jeu
     */
    private $trials = [];

    /**
     * @var JokerPlayed[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\JokerPlayed", mappedBy="user", orphanRemoval=true)
     */
    private $jokerPlayeds;

    /**
     * Liste des joker à disposition du joueur.
     */
    private array $jokers = [
    ];

    /**
     * indique le nombre de joker que le jouer peut encore jouer.
     */
    private ?int $playableJoker = null;

    /**
     * indique le nombre de points de base qu'a le joueur.
     */
    private ?int $pointsBase = null;

    public function __construct()
    {
        $this->results = new ArrayCollection();
        $this->customRitournelles = new ArrayCollection();
        $this->customResults = new ArrayCollection();
        $this->responses = new ArrayCollection();
        $this->jokerPlayeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getName(): string
    {
        if (!empty($this->firstName)) {
            return $this->firstName;
        }

        if (!empty($this->lastName)) {
            return $this->lastName;
        }

        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        if (empty($password)) {
            // Dans le cas de la mise à jour du profile sans modification du mot de passe
            return $this;
        }
        $this->passwordChanged = true;
        $this->password = $password;

        return $this;
    }

    public function hasPasswordChanged(): bool
    {
        return $this->passwordChanged;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Result[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function getTodaysResult(): ?Result
    {
        foreach ($this->results as $result) {
            if ($result->getReplyAt() > (new \DateTime('today'))) {
                return $result;
            }
        }

        return null;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setUser($this);
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getUser() === $this) {
                $result->setUser(null);
            }
        }

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(string $role): self
    {
        if (in_array($role, $this->roles)) {
            $index = array_search($role, $this->roles);
            if (false !== $index) {
                unset($this->roles[$index]);
            }
        }

        return $this;
    }

    public function isAcceptedWarn(): bool
    {
        return $this->acceptedWarn;
    }

    public function setAcceptedWarn(bool $acceptedWarn): void
    {
        $this->acceptedWarn = $acceptedWarn;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(?string $token): User
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Ritournelle[]
     */
    public function getRitournelles(): array
    {
        return $this->ritournelles;
    }

    public function addRitournelle(Ritournelle $ritournelle): self
    {
        if (!in_array($ritournelle, $this->ritournelles)) {
            $this->ritournelles[] = $ritournelle;
        }

        return $this;
    }

    public function removeRitournelle(Ritournelle $ritournelle): self
    {
        if (in_array($ritournelle, $this->ritournelles)) {
            $index = array_search($ritournelle, $this->ritournelles);
            if (false !== $index) {
                unset($this->ritournelles[$index]);
            }
        }

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|CustomRitournelle[]
     */
    public function getCustomRitournelles(): Collection
    {
        return $this->customRitournelles;
    }

    public function addCustomRitournelle(CustomRitournelle $customRitournelle): self
    {
        if (!$this->customRitournelles->contains($customRitournelle)) {
            $this->customRitournelles[] = $customRitournelle;
            $customRitournelle->setUser($this);
        }

        return $this;
    }

    public function removeCustomRitournelle(CustomRitournelle $customRitournelle): self
    {
        if ($this->customRitournelles->contains($customRitournelle)) {
            $this->customRitournelles->removeElement($customRitournelle);
            // set the owning side to null (unless already changed)
            if ($customRitournelle->getUser() === $this) {
                $customRitournelle->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CustomResult[]
     */
    public function getCustomResults(): Collection
    {
        return $this->customResults;
    }

    public function addCustomResult(CustomResult $customResult): self
    {
        if (!$this->customResults->contains($customResult)) {
            $this->customResults[] = $customResult;
            $customResult->setUser($this);
        }

        return $this;
    }

    public function removeCustomResult(CustomResult $customResult): self
    {
        if ($this->customResults->contains($customResult)) {
            $this->customResults->removeElement($customResult);
            // set the owning side to null (unless already changed)
            if ($customResult->getUser() === $this) {
                $customResult->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Response[]
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setUser($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->contains($response)) {
            $this->responses->removeElement($response);
            // set the owning side to null (unless already changed)
            if ($response->getUser() === $this) {
                $response->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Response[]
     */
    public function getTrials(Game $game): array
    {
        if (!array_key_exists($game->getId(), $this->trials)) {
            $responses = [];
            foreach ($this->responses as $response) {
                if ($response->getGame() !== $game) {
                    continue;
                }
                $responses[] = $response;
            }
            $this->trials[$game->getId()] = $responses;
        }

        return $this->trials[$game->getId()];
    }

    /**
     * @return Collection|JokerPlayed[]
     */
    public function getJokerPlayeds(): Collection
    {
        return $this->jokerPlayeds;
    }

    public function addJokerPlayed(JokerPlayed $jokerPlayed): self
    {
        if (!$this->jokerPlayeds->contains($jokerPlayed)) {
            $this->jokerPlayeds[] = $jokerPlayed;
            $jokerPlayed->setUser($this);
        }

        return $this;
    }

    public function removeJokerPlayed(JokerPlayed $jokerPlayed): self
    {
        if ($this->jokerPlayeds->contains($jokerPlayed)) {
            $this->jokerPlayeds->removeElement($jokerPlayed);
            // set the owning side to null (unless already changed)
            if ($jokerPlayed->getUser() === $this) {
                $jokerPlayed->setUser(null);
            }
        }

        return $this;
    }

    public function getJokerPlayedToday(int $joker): ?JokerPlayed
    {
        if (!array_key_exists($joker, $this->jokers)) {
            foreach ($this->jokerPlayeds as $jokerPlayed) {
                if ($jokerPlayed->getGame()->isToday()) {
                    $this->jokers[$jokerPlayed->getJoker()] = $jokerPlayed;
                }
            }
        }

        return $this->jokers[$joker];
    }

    /**
     * Indique les jokers pas encore joué, donc dispo, mais pas forcément jouable (quelque soit la raison).
     */
    public function getAvailableJoker(Game $game): int
    {
        if (is_null($this->playableJoker)) {
            // Par défaut, aucun joker n'est dispo
            $this->playableJoker = 0;
            // si le jeu n'est pas d'aujourd'hui, alors aucun joker n'est jouable
            if ($game->isToday()) {
                // si aucun essai n'a été fait, alors aucun joker n'a pu être joué
                // on commence par celui sur la similarité s'il n'a pas été joué
                if (!$game->getJokerPlayedBy($this, JokerPlayed::JOKER_SIMILARITY)) {
                    ++$this->playableJoker;
                }
                // on continue par celui sur le mot au hasard s'il n'a pas été joué
                if (!$game->getJokerPlayedBy($this, JokerPlayed::JOKER_RANDOM_WORD)) {
                    ++$this->playableJoker;
                }
            }
        }

        return $this->playableJoker;
    }

    public function getBasePoints(Game $game): int
    {
        if (is_null($this->pointsBase)) {
            $this->pointsBase = max(
                0,
                self::POINTS_BASE - count($game->getJokersPlayedBy($this))
            );
        }

        return $this->pointsBase;
    }
}
