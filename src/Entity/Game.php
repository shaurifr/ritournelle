<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    const POINTS_FIRST = 10;
    const POINTS_SECOND = 8;
    const POINTS_THIRD = 6;
    const POINTS_FOURTH = 4;
    const POINTS_OTHERS = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $playedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ritournelle", inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ritournelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Result", mappedBy="game", orphanRemoval=true)
     */
    private $results;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Response", mappedBy="game", orphanRemoval=true)
     */
    private $responses;

    /**
     * @var JokerPlayed[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\JokerPlayed", mappedBy="game", orphanRemoval=true)
     */
    private $jokerPlayeds;

    /**
     * @var bool|null
     *
     * indicates if the game is played today
     */
    private $today;

    public function __construct()
    {
        $this->results = new ArrayCollection();
        $this->responses = new ArrayCollection();
        $this->jokerPlayeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayedAt(): ?\DateTimeImmutable
    {
        return $this->playedAt;
    }

    public function setPlayedAt(\DateTimeImmutable $playedAt): self
    {
        $this->playedAt = $playedAt;

        return $this;
    }

    public function getRitournelle(): ?Ritournelle
    {
        return $this->ritournelle;
    }

    public function setRitournelle(?Ritournelle $ritournelle): self
    {
        $this->ritournelle = $ritournelle;

        return $this;
    }

    /**
     * @return Collection|Result[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setGame($this);
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getGame() === $this) {
                $result->setGame(null);
            }
        }

        return $this;
    }

    public function getNextPoints(): int
    {
        $divide = 1;
        if ((new \DateTime())->format('H') >= 12) {
            $divide = 2;
        }

        switch ($this->getResults()->count()) {
            case 0:
                return self::POINTS_FIRST / $divide;
            case 1:
                return self::POINTS_SECOND / $divide;
            case 2:
                return self::POINTS_THIRD / $divide;
            case 3:
                return self::POINTS_FOURTH / $divide;
            default:
                return self::POINTS_OTHERS / $divide;
        }
    }

    /**
     * @return Collection|Response[]
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setGame($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->contains($response)) {
            $this->responses->removeElement($response);
            // set the owning side to null (unless already changed)
            if ($response->getGame() === $this) {
                $response->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|JokerPlayed[]
     */
    public function getJokerPlayeds(): Collection
    {
        return $this->jokerPlayeds;
    }

    public function addJokerPlayed(JokerPlayed $jokerPlayed): self
    {
        if (!$this->jokerPlayeds->contains($jokerPlayed)) {
            $this->jokerPlayeds[] = $jokerPlayed;
            $jokerPlayed->setGame($this);
        }

        return $this;
    }

    public function removeJokerPlayed(JokerPlayed $jokerPlayed): self
    {
        if ($this->jokerPlayeds->contains($jokerPlayed)) {
            $this->jokerPlayeds->removeElement($jokerPlayed);
            // set the owning side to null (unless already changed)
            if ($jokerPlayed->getGame() === $this) {
                $jokerPlayed->setGame(null);
            }
        }

        return $this;
    }

    /**
     * indicates if this game is played today.
     */
    public function isToday(): bool
    {
        if (!isset($this->today)) {
            $date = (new \DateTime())->setTime(23, 59, 59);
            $interval = $this->playedAt->diff($date);

            $this->today = (0 == $interval->days);
        }

        return $this->today;
    }

    public function getJokerPlayedBy(User $user, int $joker): ?JokerPlayed
    {
        foreach ($this->jokerPlayeds as $jokerPlayed) {
            if (
                $jokerPlayed->getUser() === $user &&
                $jokerPlayed->getJoker() == $joker
            ) {
                return $jokerPlayed;
            }
        }

        return null;
    }

    /**
     * renvoie les jokers jouaient par le joueur.
     *
     * @return JokerPlayed[]
     */
    public function getJokersPlayedBy(User $user): array
    {
        $jokers = [];
        foreach ($this->jokerPlayeds as $jokerPlayed) {
            if (
                $jokerPlayed->getUser() === $user
            ) {
                $jokers[] = $jokerPlayed;
            }
        }

        return $jokers;
    }
}
