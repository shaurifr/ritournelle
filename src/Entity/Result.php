<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Result
{
    const RATING_DEFAULT = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $point;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $replyAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $comment;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $commentRitournelle;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true, length=255)
     */
    private $rating = self::RATING_DEFAULT;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $customRitournelleNotify = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoint(): ?int
    {
        return $this->point;
    }

    public function setPoint(int $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getReplyAt(): ?\DateTimeInterface
    {
        return $this->replyAt;
    }

    public function setReplyAt(\DateTimeInterface $replyAt): self
    {
        $this->replyAt = $replyAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): Result
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommentRitournelle(): ?string
    {
        return $this->commentRitournelle;
    }

    public function setCommentRitournelle(?string $commentRitournelle): Result
    {
        $this->commentRitournelle = $commentRitournelle;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): Result
    {
        $this->rating = $rating;

        return $this;
    }

    public function getCustomRitournelleNotify(): bool
    {
        return $this->customRitournelleNotify;
    }

    public function setCustomRitournelleNotify(bool $customRitournelleNotify): Result
    {
        $this->customRitournelleNotify = $customRitournelleNotify;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function setReplyAtValue()
    {
        $this->replyAt = new \DateTimeImmutable();
    }
}
