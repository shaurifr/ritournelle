<?php

namespace App\Entity;

use App\Service\NoDiacritic;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomRitournelleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CustomRitournelle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $enigma;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $response;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $clue;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="customRitournelles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var ArrayCollection|CustomResult[]
     * @ORM\OneToMany(targetEntity="App\Entity\CustomResult", mappedBy="customRitournelle", orphanRemoval=true)
     */
    private $customResults;

    public function __construct()
    {
        $this->customResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnigma(): ?string
    {
        return $this->enigma;
    }

    public function setEnigma(string $enigma): self
    {
        $this->enigma = $enigma;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getClue(): ?string
    {
        return $this->clue;
    }

    public function setClue(string $clue): self
    {
        $this->clue = $clue;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|CustomResult[]
     */
    public function getCustomResults(): Collection
    {
        return $this->customResults;
    }

    public function addCustomResult(CustomResult $customResult): self
    {
        if (!$this->customResults->contains($customResult)) {
            $this->customResults[] = $customResult;
            $customResult->setCustomRitournelle($this);
        }

        return $this;
    }

    public function removeCustomResult(CustomResult $customResult): self
    {
        if ($this->customResults->contains($customResult)) {
            $this->customResults->removeElement($customResult);
            // set the owning side to null (unless already changed)
            if ($customResult->getCustomRitournelle() === $this) {
                $customResult->setCustomRitournelle(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function isValidResponse(string $response): bool
    {
        if (empty(trim($response))) {
            return false;
        }

        return 0 === strcasecmp(
                trim(preg_replace('/[.,;:\/\!§%$£*¤"#\'`\(){}+=@&]/', '', NoDiacritic::filter($response))),
                trim(preg_replace('/[.,;:\/\!§%$£*¤"#\'`\(){}+=@&]/', '', NoDiacritic::filter($this->response)))
            );
    }

    public function __toString()
    {
        return $this->getEnigma().$this->getUser()->getUsername();
    }

    public function getCustomResult(UserInterface $user): ?CustomResult
    {
        foreach ($this->customResults as $customResult) {
            if ($customResult->getUser() === $user) {
                return $customResult;
            }
        }

        return null;
    }
}
