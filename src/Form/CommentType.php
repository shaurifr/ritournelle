<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'comment',
                TextType::class,
                [
                    'label' => 'ritournelle.comment',
                    'required' => false,
                    'attr' => [
                        'maxlength' => 100,
                        ],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'comment_ritournelle',
                TextType::class,
                [
                    'label' => 'ritournelle.comment_ritournelle',
                    'required' => false,
                    'attr' => [
                        'maxlength' => 500,
                        ],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'rating',
                HiddenType::class,
                [
                    'label' => 'ritournelle.rating',
                    'required' => false,
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'acceptedWarn',
                CheckboxType::class,
                [
                    'label' => 'ritournelle.acceptedWarn',
                    'required' => false,
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'ritournelle.save_comment',
                    'translation_domain' => 'ritournelle',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
            ]
        );
    }
}
