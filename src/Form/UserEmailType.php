<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'user.forgottenPassword.email.modal',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 250,
                        'placeholder' => 'yoda@ritournelle.fr',
                    ],
                    'translation_domain' => 'user',
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 3]),
                        new Email(),
                    ],
                ]
            )
            ->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'user.forgottenPassword.send.modal',
                    'translation_domain' => 'user',
                    'attr' => [
                        'class' => 'btn btn-primary',
                    ],
                ]
            )
            ->add(
                'close',
                ButtonType::class,
                [
                    'label' => 'user.forgottenPassword.close.modal',
                    'translation_domain' => 'user',
                    'attr' => [
                        'data-dismiss' => 'modal',
                        'class' => 'btn btn-secondary',
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
