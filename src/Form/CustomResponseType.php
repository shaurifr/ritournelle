<?php

namespace App\Form;

use App\Entity\CustomRitournelle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomResponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'customRitournelle',
                EntityType::class,
                [
                    'class' => CustomRitournelle::class,
                ]
            )
            ->add(
                'response',
                TextType::class,
                [
                    'label' => 'ritournelle.response',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 1024,
                        'placeholder' => 'ritournelle.placeholder_response',
                        'autofocus' => '',
                    ],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'ritournelle.save',
                    'translation_domain' => 'ritournelle',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                // Configure your form options here
            ]
        );
    }
}
