<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'user.username',
                    'required' => true,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                    'disabled' => true,
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'user.first_name',
                    'required' => false,
                    'attr' => ['maxlength' => 25],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'user.last_name',
                    'required' => false,
                    'attr' => ['maxlength' => 30],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'user.email',
                    'required' => true,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label' => 'user.password',
                    'required' => false,
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'acceptedWarn',
                CheckboxType::class,
                [
                    'label' => 'user.accepted_warn',
                    'required' => false,
                    'translation_domain' => 'user',
                ]
            )
          ->add(
            'save',
            SubmitType::class,
            [
              'label' => 'user.register_profile',
              'translation_domain' => 'user',
            ]
          );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }
}
