<?php

namespace App\EventListener;

use App\Entity\CustomRitournelle;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\CustomRitournelleManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Psr\Log\LoggerInterface;

class NotifySubscriber implements EventSubscriber
{
    private CustomRitournelleManager $customRitournelleManager;
    private LoggerInterface $logger;

    public function __construct(
        CustomRitournelleManager $customRitournelleManager,
        LoggerInterface $logger
    ) {
        $this->customRitournelleManager = $customRitournelleManager;
        $this->logger = $logger;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    /**
     * @throws \Exception
     */
    public function postPersist(PostPersistEventArgs $args)
    {
        $entity = $args->getObject();

        // À chaque nouvelle insertion d'une customRitournelle, on prévient tous ceux qui veulent être prévenus
        if ($entity instanceof CustomRitournelle) {
            /** @var CustomRitournelle $entity */

            /** @var EntityManagerInterface $entityManager */
            $entityManager = $args->getObjectManager();
            // on récupère tous ceux qui veulent être notifié de la nouvelle Ritournelle
            /** @var UserRepository $userRepository */
            $userRepository = $entityManager->getRepository(User::class);
            $users = $userRepository->findToBeNotified();

            // on envoie un email à chaque user
            $this->logger->debug(count($users).' will be notified');
            foreach ($users as $user) {
                if ($user == $entity->getUser()) {
                    // on ne prévient le créateur de la ritournelle de sa propre ritournelle
                    continue;
                }
                $this->logger->debug('we notify '.$user->getEmail());
                $this->customRitournelleManager->notify($entity, $user);
            }
        }
    }
}
