<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\JokerPlayed;
use App\Entity\Response;
use App\Entity\Result;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class JokerManager
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $entityManager;
    /** @var LoggerInterface */
    private LoggerInterface $logger;
    /** @var TranslatorInterface */
    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->logger = $logger;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function getClue(int $joker, User $user, Game $game): string
    {
        switch ($joker) {
            case JokerPlayed::JOKER_SIMILARITY:
                // on s'assure que le joueur a déjà un essai pour jouer ce joker
                $trials = $user->getTrials($game);
                if (empty($trials)) {
                    $this->logger->error(
                        'Unable to play Joker',
                        [
                            'nbTrials' => count($trials),
                            'idJoker' => $joker,
                            'idUser' => $user->getId(),
                            'idGame' => $game->getId(),
                        ]
                    );
                    throw new \Exception('Unable to play Joker');
                }

                return $this->closestTrial($trials, $game);
            case JokerPlayed::JOKER_RANDOM_WORD:
                if (!$game->getJokerPlayedBy($user, JokerPlayed::JOKER_SIMILARITY)) {
                    $this->logger->error(
                        'Joker not allowed to be played',
                        [
                            'idJoker' => $joker,
                            'idUser' => $user->getId(),
                            'idGame' => $game->getId(),
                        ]
                    );
                    throw new \Exception('Joker not allowed to be played');
                }
                $randomWords = $game->getRitournelle()->getRandomHalfWords();

                return $this->translator->trans(
                    'ritournelle.joker.clue.randomWord',
                    [
                        'response' => implode(', ', $randomWords),
                        'nb' => count($randomWords),
                    ],
                    'ritournelle');
            case JokerPlayed::JOKER_GET_ANSWER:
                $hour = (new \DateTime())->format('H');
                if (
                    !$game->getJokerPlayedBy($user, JokerPlayed::JOKER_SIMILARITY) ||
                    !$game->getJokerPlayedBy($user, JokerPlayed::JOKER_SIMILARITY) ||
                    $hour < GameManager::HOUR_CLUE
                ) {
                    $this->logger->error(
                        'Joker not allowed to be played',
                        [
                            'idJoker' => $joker,
                            'idUser' => $user->getId(),
                            'idGame' => $game->getId(),
                        ]
                    );
                    throw new \Exception('Joker not allowed to be played');
                }
                $response = $game->getRitournelle()->getResponse();
                // comme on va donner la réponse à l'utilisateur, on enregistre en base qu'il a trouvé (et qu'il n'a pas de point)
                $result = new Result();
                $result->setPoint(0);
                $result->setUser($user);
                $result->setGame($game);
                $this->entityManager->persist($result);
                $this->entityManager->flush();

                return $this->translator->trans(
                    'ritournelle.joker.clue.get_answer',
                    [
                        'response' => $response,
                    ],
                    'ritournelle');
            default:
                $this->logger->error(
                    'Unknown Joker',
                    [
                        'idJoker' => $joker,
                        'idUser' => $user->getId(),
                        'idGame' => $game->getId(),
                    ]
                );
                throw new \Exception('Unknwon Joker');
        }
    }

    private function closestTrial(array $trials, Game $game): string
    {
        /** @var Response[] $trials */
        $clue = '';
        $bestSimilary = 0;
        foreach ($trials as $trial) {
            $similarity = $game->getRitournelle()->getSimilarity($trial->getResponse());

            if ($similarity > $bestSimilary) {
                $bestSimilary = $similarity;
                $clue = $this->translator->trans('ritournelle.joker.clue.similarity', ['response' => $trial->getResponse(), 'similarity' => $similarity], 'ritournelle');
            }
        }

        return $clue;
    }
}
