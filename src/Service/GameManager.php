<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\Result;
use App\Entity\Ritournelle;
use App\Repository\GameRepository;
use App\Repository\ResultRepository;
use App\Repository\RitournelleRepository;
use Doctrine\ORM\EntityManagerInterface;

class GameManager
{
    const HOUR_START = 9;
    const HOUR_CLUE = 12;
    const HOUR_END = 18;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Result[] */
    private $currentWeekResults;

    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $firstCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $secondCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $thirdCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $othersCurrentWeek = null;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFirstCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->firstCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->firstCurrentWeek;
    }

    public function getSecondCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->secondCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->secondCurrentWeek;
    }

    public function getThirdCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->thirdCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->thirdCurrentWeek;
    }

    public function getOthersCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->othersCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->othersCurrentWeek;
    }

    public function getFirstCurrentMonth(): array
    {
        return [];
    }

    public function getSecondCurrentMonth(): array
    {
        return [];
    }

    public function getThirdCurrentMonth(): array
    {
        return [];
    }

    public function getOthersCurrentMonth(): array
    {
        return [];
    }

    /**
     * @return Result[]
     */
    public function getCurrentWeekResults(): array
    {
        if (empty($this->currentWeekResults)) {
            /** @var ResultRepository $resultRepository */
            $resultRepository = $this->entityManager->getRepository(Result::class);
            $this->currentWeekResults = $resultRepository->findCurrentWeek();
        }

        return $this->currentWeekResults;
    }

    /**
     * @return Game
     *
     * @throws \Exception
     */
    public function getTodaysGame()
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = $this->entityManager->getRepository(Game::class);

        $result = $gameRepo->findBy(['playedAt' => new \DateTimeImmutable('today')]);

        if (!empty($result)) {
            return array_pop($result);
        }
        // Aucune Ritournelle n'a été encore jouée aujourd'hui
        // on en choisie une au hasard qui n'a pas été jouée depuis 1 mois
        /** @var RitournelleRepository $ritournelleRepo */
        $ritournelleRepo = $this->entityManager->getRepository(Ritournelle::class);
        $times = 1;
        $limit = 5;
        do {
            $ritournelles = $ritournelleRepo->findPlayedLessThanXTimesAndNotPlayedSince($times, '-2 month');
            ++$times;
            if ($times > 100) {
                --$limit;
            }
        } while (count($ritournelles) < $limit);
        $index = rand(0, count($ritournelles) - 1);
        $ritournelle = $ritournelles[$index];

        $game = new Game();
        $game->setPlayedAt(new \DateTimeImmutable('today'));
        $game->setRitournelle($ritournelle);
        $this->entityManager->persist($game);
        $this->entityManager->flush();

        return $game;
    }

    public function getPreviousGame(): ?Game
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = $this->entityManager->getRepository(Game::class);

        return $gameRepo->getPreviousGame();
    }

    private function calculateCurrentWeek(): array
    {
        $ranking = $this->calculateRanking(
            $this->getCurrentWeekResults()
        );

        $this->firstCurrentWeek = $ranking['1'];
        $this->secondCurrentWeek = $ranking['2'];
        $this->thirdCurrentWeek = $ranking['3'];
        $this->othersCurrentWeek = $ranking['others'];

        return $ranking;
    }

    public function calculateLastWeek(): array
    {
        /** @var ResultRepository $resultRepository */
        $resultRepository = $this->entityManager->getRepository(Result::class);

        $ranking = $this->calculateRanking(
            $resultRepository->findLastWeek()
        );

        return $ranking;
    }

    private function calculateRanking(array $results): array
    {
        $calculatedResults = [];
        $ranking = [
            '1' => [], '2' => [], '3' => [], 'others' => [],
        ];

        foreach ($results as $result) {
            $user = $result->getUser();
            $idUser = $user->getId();
            $points = $result->getPoint();
            if (!array_key_exists($idUser, $calculatedResults)) {
                $calculatedResults[$idUser] = ['user' => $user, 'points' => 0, 'id' => $idUser];
            }
            $calculatedResults[$idUser]['points'] += $points;
        }
        usort($calculatedResults, function ($a, $b) {
            return $a['points'] <=> $b['points'];
        });

        while (!empty($calculatedResults)) {
            $entry = array_pop($calculatedResults);
            if (empty($ranking['1']) || $ranking['1'][0]['points'] == $entry['points']) {
                $ranking['1'][$entry['id']] = $entry;
                continue;
            }
            if (empty($ranking['2']) || $ranking['2'][0]['points'] == $entry['points']) {
                $ranking['2'][$entry['id']] = $entry;
                continue;
            }
            if (empty($ranking['3']) || $ranking['3'][0]['points'] == $entry['points']) {
                $ranking['3'][$entry['id']] = $entry;
                continue;
            }
            $ranking['others'][$entry['id']] = $entry;
        }

        return $ranking;
    }
}
