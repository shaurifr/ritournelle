<?php

namespace App\DataFixtures;

use App\Entity\Ritournelle;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RitournelleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->getReference(UserFixtures::ADMIN_USER_REFERENCE);

        $ritournelle1 = new Ritournelle();
        $ritournelle1->setEnigma('La grande alarme');
        $ritournelle1->setResponse('La petite sirène');
        $ritournelle1->setClue('Ça nage dans l\'eau');
        $ritournelle1->setUser($user);
        $manager->persist($ritournelle1);

        $ritournelle2 = new Ritournelle();
        $ritournelle2->setEnigma('Le bol de terre');
        $ritournelle2->setResponse('La coupe du monde');
        $ritournelle2->setClue('C\'est pour jouer');
        $ritournelle2->setUser($user);
        $manager->persist($ritournelle2);

        $ritournelle3 = new Ritournelle();
        $ritournelle3->setEnigma('Le petit rouge');
        $ritournelle3->setResponse('Le grand bleu');
        $ritournelle3->setClue('Ça va profond');
        $ritournelle3->setUser($user);
        $manager->persist($ritournelle3);

        $ritournelle4 = new Ritournelle();
        $ritournelle4->setEnigma('La terre du pasteur');
        $ritournelle4->setResponse('L\'étoile du berger');
        $ritournelle4->setClue('Les marins l\'adorent');
        $ritournelle4->setUser($user);
        $manager->persist($ritournelle4);

        $ritournelle5 = new Ritournelle();
        $ritournelle5->setEnigma('Earth Peace');
        $ritournelle5->setResponse('Star Wars');
        $ritournelle5->setClue('ne sait pas compter');
        $ritournelle5->setUser($user);
        $manager->persist($ritournelle5);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
