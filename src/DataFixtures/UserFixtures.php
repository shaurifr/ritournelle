<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const ADMIN_USER_REFERENCE = 'admin-user';

    /** @var UserPasswordEncoderInterface $passwordEncoder */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('carlos@shauri.fr');
        $user->setUsername('carlos');
        $user->setFirstName('Carlos');
        $user->setLastName('Pereira');
        $user->addRole('ROLE_ADMIN');
        $password = $this->passwordEncoder->encodePassword($user, 'carlos');
        $user->setPassword($password);
        $manager->persist($user);

        $this->addReference(self::ADMIN_USER_REFERENCE, $user);

        $user = new User();
        $user->setEmail('carlos+user@shauri.fr');
        $user->setUsername('carlos2');
        $user->setFirstName('Carlos2');
        $user->setLastName('Pereira2');
        $password = $this->passwordEncoder->encodePassword($user, 'carlos2');
        $user->setPassword($password);
        $manager->persist($user);

        $otherUsers = ['edward', 'oswald', 'selina', 'harvey', 'ivy', 'jack', 'victor'];
        foreach ($otherUsers as $u) {
            $user = new User();
            $user->setEmail($u.'@test.fr');
            $user->setUsername($u);
            $user->setFirstName($u);
            $user->setLastName($u);
            $password = $this->passwordEncoder->encodePassword($user, $u);
            $user->setPassword($password);
            $manager->persist($user);
        }
            
        $manager->flush();
    }
}
