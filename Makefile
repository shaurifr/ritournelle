OS := $(shell uname)

.DEFAULT_GOAL := help
help:
		@grep -E '(^[1-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

reset-db: ## sync force the database with the schema
	bin/console doctrine:database:drop --force --if-exists
	bin/console doctrine:database:create
	bin/console doctrine:migration:migrate -n
	bin/console doctrine:schema:validate
	bin/console doctrine:fixtures:load -n -e dev

quality: ## check and enforce quality of the project
	bin/console doctrine:schema:validate
	php -d memory_limit=-1 vendor/bin/php-cs-fixer fix --rules=@Symfony
	php -d memory_limit=-1 vendor/bin/phpstan analyse -l 5 -c phpstan.neon src

db: ## migrate the database with the schema
	bin/console doctrine:migration:migrate

update-translation-fr: ## updates the file with the missing translations
	bin/console translation:update --dump-messages --force fr
