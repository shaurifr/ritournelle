window.addEventListener('load', e => {
//    new PWAConfApp();
    registerSW();
});

async function registerSW() {
    if ('serviceWorker' in navigator) {
        try {
            await navigator.serviceWorker.register('./sw.js');
        } catch (e) {
            console.log('ServiceWorker registration failed. Sorry about that.');
        }
    } else {
//        document.querySelector('.alert').removeAttribute('hidden');
    }
}
