const cacheName = 'ritournelle-v1';
const staticAssets = [
    './',
    './register',
    './connect',
    './ritournelle/enigma',
    './game',
    './js/app.js',
    './css/main.css',
    './img/ritournelle.png'
];

self.addEventListener('install', async event => {
    const cache = await caches.open(cacheName);
    await cache.addAll(staticAssets);
});

self.addEventListener('fetch', async event => {
    const req = event.request;

    if (/.*(\.css|js|png|jpg|jpeg|gif|svg)$/.test(req.url)) {
        event.respondWith(cacheFirst(req));
    } else {
        event.respondWith(networkFirst(req));
    }
});

async function cacheFirst(req) {
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(req);
/*
    if (cachedResponse) {
        console.log(req.url);
        console.log(cachedResponse.headers.get('last-modified'));
        lastModifiedAt = new Date(cachedResponse.headers.get('last-modified'));
        console.log(lastModifiedAt);
        console.log(cachedResponse.headers.forEach(function (value, key) {
            console.log(key);
            console.log(value);
        }));
    }
 */
    return cachedResponse || networkFirst(req);
}

async function networkFirst(req) {
    const cache = await caches.open(cacheName);
    try {
        const fresh = await fetch(req);
        cache.put(req, fresh.clone());
        return fresh;
        // @todo on désaffiche qu'on est hors-ligne
    } catch (e) {
        // @todo on affiche qu'on est hors-ligne
        cachedResponse = await cache.match(req);
        return cachedResponse;
    }
}
